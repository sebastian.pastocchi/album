# Album
# AlbumAplicacion para mostrar albunes de fotos.

...
**Clonar repositorio**
```
$ git clone git@gitlab.com:sebastian.pastocchi/album.git
```
cd album

**Crear imagen**
```
$ sudo docker build -t album-image .
```
**Desplegar contenedor**
```
$ docker run -d -p 80:80 album-image
```
